package com.example.tvmaze.mazeConfig;

import com.example.tvmaze.interfaces.TvMazeService;

import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

public class TvMazeConfig {

    private static Retrofit retrofit;

    public TvMazeConfig(String base) {

        this.retrofit = new Retrofit.Builder()
                            .baseUrl(base)
                            .addConverterFactory(JacksonConverterFactory.create())
                            .build();

    }

    public TvMazeService getTvMazeService(){

        return this.retrofit.create(TvMazeService.class);
    }
}
