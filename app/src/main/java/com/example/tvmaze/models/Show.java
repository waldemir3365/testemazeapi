package com.example.tvmaze.models;

import java.util.List;

public class Show {

    private List<Filme> filmes;

    public List<Filme> getFilmes() {
        return filmes;
    }

    public void setFilmes(List<Filme> filmes) {
        this.filmes = filmes;
    }
}
