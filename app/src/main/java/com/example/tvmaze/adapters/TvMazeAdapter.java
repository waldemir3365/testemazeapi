package com.example.tvmaze.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.tvmaze.R;
import com.example.tvmaze.holders.TvMazeViewHolder;
import com.example.tvmaze.interfaces.TvMazeOnclickListener;
import com.example.tvmaze.models.Filme;

import java.util.List;

public class TvMazeAdapter extends RecyclerView.Adapter {


    private Context context;
    private List<Filme> listFilme;
    private TvMazeOnclickListener listener;

    public TvMazeAdapter(Context context, List<Filme> filmes, TvMazeOnclickListener listener) {

        this.context = context;
        this.listFilme = filmes;
        this.listener = listener;

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(context).inflate(R.layout.row_list_tvmaze, viewGroup, false);

        return new TvMazeViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {

        TvMazeViewHolder holder = (TvMazeViewHolder) viewHolder;

        Filme f = listFilme.get(i);
        holder.setFields(f,listener);

    }

    @Override
    public int getItemCount() {
        return listFilme.size();
    }
}
