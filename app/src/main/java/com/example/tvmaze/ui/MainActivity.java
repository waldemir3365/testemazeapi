package com.example.tvmaze.ui;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.example.tvmaze.R;
import com.example.tvmaze.adapters.TvMazeAdapter;
import com.example.tvmaze.constants.Constants;
import com.example.tvmaze.fragments.DetailsFragment;
import com.example.tvmaze.interfaces.TvMazeOnclickListener;
import com.example.tvmaze.mazeConfig.TvMazeConfig;
import com.example.tvmaze.models.Filme;
import com.example.tvmaze.models.Show;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {


    ViewHolder mViewHolder = new ViewHolder();
    private FrameLayout frame;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mViewHolder.recycler = findViewById(R.id.recycler);
        frame = findViewById(R.id.frame_layout);
        init();
    }

    private void init(){

        getCall();

    }

    private void getCall() {
        Call<List<Filme>> call = new TvMazeConfig(Constants.URL_BASE_TvMaze).getTvMazeService().getShows();
        call.enqueue(getServiceCall());
    }

    private Callback<List<Filme>> getServiceCall() {

        return new Callback<List<Filme>>() {
            @Override
            public void onResponse(Call<List<Filme>> call, Response<List<Filme>> response) {


                Show show = new Show();
                show.setFilmes(response.body());
                //setAdapter List
                setAdapterList(show.getFilmes());

            }

            @Override
            public void onFailure(Call<List<Filme>> call, Throwable t) {

            Toast.makeText(getBaseContext(), "ruim" + t.getMessage(), Toast.LENGTH_LONG).show();
            Log.d("error", t.getMessage());
            Log.e("error", t.getMessage());

            }
        };
    }

    private void setAdapterList(List<Filme> filmes) {
        mViewHolder.recycler.setAdapter(new TvMazeAdapter(getBaseContext(),filmes,clicklist()));
        setLayoutList();
    }

    private TvMazeOnclickListener clicklist() {
        return new TvMazeOnclickListener() {
            @Override
            public void OnclickListener(Filme f) {
                Toast.makeText(getBaseContext(),f.getName(), Toast.LENGTH_LONG).show();
                //trocar tela fragment
                NavigationFragment();
            }
        };
    }

    private void NavigationFragment() {

        DetailsFragment detailsFragment = new DetailsFragment();

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.frame_layout, detailsFragment, "TAG");
        ft.addToBackStack(null);
        ft.commit();
    }

    private void setLayoutList() {

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        mViewHolder.recycler.setLayoutManager(layoutManager);

    }

    public class ViewHolder{

        RecyclerView recycler;
    }
}
