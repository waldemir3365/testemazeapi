package com.example.tvmaze.interfaces;

import com.example.tvmaze.models.Filme;
import com.example.tvmaze.models.Show;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface TvMazeService {


    @GET("singlesearch/shows")
    Call<Filme> getFilme(@Query("q") String param);

    @GET("shows")
    Call<List<Filme>> getShows();


}
