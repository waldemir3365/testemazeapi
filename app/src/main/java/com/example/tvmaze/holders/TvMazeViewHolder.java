package com.example.tvmaze.holders;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.example.tvmaze.R;
import com.example.tvmaze.interfaces.TvMazeOnclickListener;
import com.example.tvmaze.models.Filme;

public class TvMazeViewHolder extends RecyclerView.ViewHolder {

    private TextView nome;

    public TvMazeViewHolder(View view) {
        super(view);
        nome = view.findViewById(R.id.text_nome);
    }

    public void setFields(Filme f, TvMazeOnclickListener listener) {

        nome.setText(f.getName());
        nome.setOnClickListener(clickNameList(f,listener));
    }

    private View.OnClickListener clickNameList(Filme f, TvMazeOnclickListener listener) {
        return view->{
            listener.OnclickListener(f);
        };
    }
}

